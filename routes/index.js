var express = require('express');
var router = express.Router();
var User = require('../modles/user');
var Constellation = require('../modles/constellation');

/* GET home page. */
router.get('/', function(req, res) {
	res.render('index', { title: 'Astrology' });
});

/* sign in */
router.get('/signin', function(req, res) {
	res.render('signin', {title: 'Sign in'});
});

router.post('/signin', function(req, res) {
	User.get(req.body.username, function(err, user){
		if(!user){
			req.flash('error', 'Username doesn\'t exists');
			return res.redirect('/signin');
		}
		if(user.password != req.body.password){
			req.flash('error', 'Password is wrong');
			return res.redirect('/signin');
		}
		req.session.loginUser = user;
		req.flash('success','Sign in success');
		res.redirect('/');
	});
});

/* sign up */
router.post('/signup', function(req, res){
	var password = req.body.password;
	var repassword = req.body.repassword;
	if (password != repassword) {
		req.flash('error', 'password mismatch');
		return res.redirect('/signup');
	}
	var user = new User({
		username: req.body.username,
		password: password
	});
	User.get(user.username, function(err, doc){
		if(doc){
			err = 'username already exists';
		}
		if(err){
			req.flash('error', err);
			return res.redirect('/signup');
		}
		user.save(function(err){
			if(err){
				req.flash('error', err);
				return res.redirect('/signup');
			}
			req.flash('success', 'Sign up success, please login.');
			return res.redirect('/signin');
		});
	});
});

/* user info */
router.get('/userinfo', function(req, res){
	Constellation.getAll(function(err, docs){
		if(err){
			req.flash('error', err);
		}
    	res.render('userinfo', {
    		title: 'user info',
    		constellations: docs
    	});
	});
});

/* update userinfo */
router.post('/updateProile', function(req, res){
	var select = {username: req.session.loginUser.username};
	var constellation = req.body.constellation;
	var update = {constellation: constellation};
	User.update(select, update, function(err, result){
		if(err){
			req.flash('error', err);
			return res.redirect('/userinfo');
		}
		console.log(result);
		if(result.result.ok == '1' && result.result.n > 0){
			req.flash('success', 'user info updated successfully.');
			User.get(req.session.loginUser.username, function(err2, doc){
				if(err2){
					req.flash('error', err2);
					return res.redirect('/userinfo');
				}
				req.session.loginUser = doc;
				return res.redirect('/userinfo');
			});
		}else{
			req.flash('success', 'user info updated failed');
			return res.redirect('/userinfo');
		}
	});
});

/* update password */
router.post('/updatePassword', function(req, res){
	var newPassword = req.body.newPassword;
	var reNewPassword = req.body.reNewPassword;
	if(reNewPassword != newPassword){
		req.flash('error', 'password dismatch!');
		return res.redirect('/userinfo');
	}
	User.get(req.session.loginUser.username, function(err, user){
		if(user.password != req.body.oldPassword){
			err = 'password is wrong';
		}
		if(err){
			req.flash('error', err);
			return res.redirect('/userinfo');
		}
		var select = {username: req.session.loginUser.username};
		var update = {password: newPassword};
		User.update(select, update, function(err2, result){
			if(err2){
				req.flash('error', err2);
				return res.redirect('/userinfo');
			}
			if(result.result.ok == '1' && result.result.nModified > 0){
				req.flash('success', 'password updated successfully');
			}else{
				req.flash('success', 'password udpated failed');
			}
			return res.redirect('/userinfo');
		});
	});
});

router.post('/delete', function(req, res){
	var username = req.session.loginUser.username;
	var select = {username: username};
	User.delete(select, function(err, result){
		if(err){
			req.flash('error', err);
			return res.redirect('/userinfo');
		}
		if(result.result.ok == '1' && result.result.n > 0){
			req.flash('success', 'Account removed successfully');
			req.session.loginUser = null;
			return res.redirect('/');
		}else{
			req.flash('success', 'Account removed failed');
			return res.redirect('/userinfo');
		}
	});
});

/* sign out */
router.get('/signout', function(req, res){
	req.session.loginUser = null;
	req.flash('success', 'Sign out success');
	res.redirect('/');
});

module.exports = router;