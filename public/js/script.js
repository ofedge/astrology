$(function(){
	script.signup();
	$('select').select2({dorpdownCssClass:'dropdown-inverse'});
	script.userinfo();
});
var script = {
		signup: function(){
			$('#signUpBtn').on('click', function(){
				$('div.signin-form').fadeOut('fast', function(){
					$('div.signup-form').fadeIn('fast');
				});
			});
			$('#signInBtn').on('click', function(){
				$('div.signup-form').fadeOut('fast', function(){
					$('div.signin-form').fadeIn('fast');
				});
			});
		},
		userinfo: function(){
			$('#deleteAccountModal').find('#username').on('keyup', function(){
				if($(this).val() == $(this).attr('hover-data')){
					$('#deleteAccountModal').find('button').removeAttr('disabled');
				} else {
					$('#deleteAccountModal').find('button').attr('disabled', 'disabled');
				}
			});
			$('#deleteAccountModal').on('hidden.bs.modal', function(){
				$(this).find('#username').val('');
			});
		}
}
