var mongodb = require('./db');

function Constellation(constellation){
	this.id = constellation._id;
	this.name_en = constellation.name_en;
	this.name_zh = constellation.name_zh;
	this.birth_from = constellation.birth_from;
	this.birth_to = constellation.birth_to;
}
module.exports = Constellation;

Constellation.getAll = function(callback){
	mongodb.open(function(err, db){
		if(err){
			return callback(err);
		}
		db.collection('constellation', function(err, collection){
			if(err){
				mongodb.close();
				return callback(err);
			}
			collection.find().toArray(function(err, docs){
				mongodb.close();
				if(docs){
					var arr = new Array();
					for(var i = 0; i < docs.length; i++){
						var constellation = new Constellation(docs[i]);
						arr.push(constellation);
					}
					return callback(err, arr);
				}else{
					return callback(err, null);
				}
			});
		});
	});
}
