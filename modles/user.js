var mongodb = require('./db');

function User(user) {
	this._id = user._id;
	this.username = user.username;
	this.password = user.password;
	this.constellation = user.constellation;
}
module.exports = User;

/* add user */
User.prototype.save = function(callback){
	var user = {
	    username: this.username,
	    password: this.password
	}
	mongodb.open(function(err, db){
		if(err){
			return callback(err);
		}
		db.collection('user', function(err, collection){
			if(err){
				mogodb.close();
				return callback(err);
			}
			collection.insert(user, {}, function(err, result){
				mongodb.close();
				callback(err, user);
			});
		});
	});
}

/* get user by username */
User.get = function(username, callback){
	mongodb.open(function(err, db){
		if(err){
			return callback(err);
		}
		db.collection('user', function(err, collection){
			if(err){
				mongodb.close();
				return callback(err);
			}
			collection.findOne({username: username}, function(err, doc){
				mongodb.close();
				if(doc){
					var user = new User(doc);
					return callback(err, user);
				}else{
					return callback(err, null);
				}
			});
		});
	});
}

/* update */
User.update = function(select, update, callback){
	mongodb.open(function(err, db){
		if(err){
			return callback(err);
		}
		db.collection('user', function(err, collection){
			if(err){
				mongodb.close();
				return callback(err);
			}else{
				collection.update(select, {$set: update}, {safe: true}, function(err, result){
					mongodb.close();
					if(err){
						return callback(err);
					}
					return callback(null, result);
				});
			}
		});
	});
}

/* delete */
User.delete = function(select, callback){
	mongodb.open(function(err, db){
		if(err){
			return callback(err);
		}
		db.collection('user', function(err, collection){
			if(err){
				mongodb.close();
				return callback(err);
			}
			collection.remove(select, {single: true}, function(err, result){
				mongodb.close();
				if(err){
					return callback(err);
				}
				return callback(null, result);
			});
		});
	});
}