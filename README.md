永远记得要处理err...永远记得要关数据库...

***
mongodb备份

    mongodump -h dbhost -d dbname -o dbdirectory

-h mongodb所在服务器, 比如: 127.0.0.1, 也可以指定端口号: 127.0.0.1:27017

-d: 要备份的数据库实例, 如test

-o 备份存放位置, 比如d:\bakcup\mongodb

mongodb恢复

    mongorestore -h dbhost -d dbname -directoryperdb dbdirectory
    
-h mongodb所在服务器地址

-d 要恢复的数据库实例, 可以与本分时候不同

-directoryperdb 备份数据库所在位置, 比如d:\backup\mongodb\test,  test指备份的数据库实例文件夹

-drop 导入collection之前先删除

mongodb 查询

    db.collection.find();
    db.collection.findOne();
    
mongodb 插入

    db.collection.insert(doc);
    
mongodb 修改

    db.collection.update();
    $set, $inc, $push, $unset...
    
mongodb 删除

1.数据库

    use test;
    db.dropDatabase();

2.collection

    db.collection.drop();

***

打开数据库

    mogodb.open(function(err, db){});

打开一个collection

    db.collection('collectionName', function(err, collection){});

插入数据

    collection.save(doc, options, function(err, result){});
    collection.insert(doc, options, function(err, result){});

***

jade模板中调用session的问题, 所有放在session中的数据都要在下面的方法中设置一遍(是不是有点麻烦)

    app.use(function(req, res, next){
        app.locals.user = req.session.user;
        next();
    });
